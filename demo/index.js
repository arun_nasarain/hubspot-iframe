//import CallingExtensions, { Constants } from "@hubspot/calling-extensions-sdk";

import CallingExtensions from "../src/CallingExtensions";
//import { errorType } from "../src/Constants";
const callback = () => {

  const defaultSize = {
    width: 400,
    height: 500
  };

  const state = {};

  const cti = new CallingExtensions({
    debugMode: true,
    eventHandlers: {
      onReady: () => {
        console.log("onready");
        cti.initialized({
          isLoggedIn: true,
          sizeInfo: defaultSize
        });
      },
      onDialNumber: (data, rawEvent) => {
        document.getElementById("calldetails").style.display = "none";
        document.getElementById("calls").style.display = "block";
        document.getElementById("footer").style.display = "none";


        console.log("ondialnumber");
        var ele=document.getElementById("endcall");
        console.log(ele);
        console.log(rawEvent);
        const { phoneNumber } = data;
        state.phoneNumber = phoneNumber;
        document.getElementById("mobile").innerHTML = phoneNumber;
        cti.outgoingCall({
          createEngagement: true,
          phoneNumber
        })
        ele.onclick=endcall;

      },
      onEngagementCreated: (data, rawEvent) => {
        const { engagementId } = data;
        state.engagementId = engagementId;
      },
      onVisibilityChanged: (data, rawEvent) => {
        console.log(data)
      }
    }
  });

function endcall(){
    cti.callEnded({
      engagementId: state.engagementId
    });
    console.log("call ended")
    document.getElementById("calls").style.display = "none";
    document.getElementById("footer").style.display = "block";
    document.getElementById("calldetails").style.display = "block";
    var ele1=document.getElementById("save");
    //window.location ="./callLog.html";
console.log(ele1)
    ele1.onclick=completecall;
        
    //window.setTimeout(() => window.location ="./callLog.html", 3000);
    //document.getElementById("call").style.display = "none";

    //window.location ="./callLog.html";
 }

 function completecall(){
  
      
  cti.callCompleted({
    engagementId: state.engagementId,
    hideWidget:true
  });
    console.log("call completed")
  
}

};

//callback()
if (
  document.readyState === "interactive" ||
  document.readyState === "complete"
) {
  window.setTimeout(() => callback(), 1000);
} else {
  document.addEventListener("DOMContentLoaded", callback);
}
